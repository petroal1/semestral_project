// GENERATED AUTOMATICALLY FROM 'Assets/Input/InputManager.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputManager : IInputActionCollection, IDisposable
{
    private InputActionAsset asset;
    public @InputManager()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputManager"",
    ""maps"": [
        {
            ""name"": ""Player1"",
            ""id"": ""04c1eda6-9514-4ad5-a486-ef8942563ff2"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""80113588-9cdf-44f4-b97d-f7eac156f533"",
                    ""expectedControlType"": ""Dpad"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LightPunch"",
                    ""type"": ""Button"",
                    ""id"": ""f08eb1a4-2e27-4f4f-9158-863a3567559e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LightKick"",
                    ""type"": ""Button"",
                    ""id"": ""9387f331-803d-4e1a-8c68-45c7fdd9a603"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""HardPunch"",
                    ""type"": ""Button"",
                    ""id"": ""9b985bc7-bd58-43fd-b0c7-58382d04844f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""HardKick"",
                    ""type"": ""Button"",
                    ""id"": ""198a8a45-27a8-4d25-96d3-aad48c2c8764"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""ArrowKeys"",
                    ""id"": ""0cd09519-25e8-450a-b87c-4762740cc836"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""a8884d9c-3f82-49d5-8d4f-b31aa140436b"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""25c7ceaa-4097-4176-ad2a-5be47805053d"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""1cd1241f-68df-456d-8542-bf792dde33c7"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""96d74614-b3b2-4b88-9a5e-d7341b5b508e"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""6f3d79d5-acaf-486c-b6e1-20793c67e4f7"",
                    ""path"": ""<Keyboard>/z"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LightPunch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1395fe1d-8ceb-4536-b066-5be08e3759ea"",
                    ""path"": ""<Keyboard>/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LightKick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ef0da734-3f2d-4df7-9d7a-d66fa2696cb9"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HardPunch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""90b1603b-f20d-4124-a850-0bd923049eb2"",
                    ""path"": ""<Keyboard>/leftAlt"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HardKick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Player1
        m_Player1 = asset.FindActionMap("Player1", throwIfNotFound: true);
        m_Player1_Move = m_Player1.FindAction("Move", throwIfNotFound: true);
        m_Player1_LightPunch = m_Player1.FindAction("LightPunch", throwIfNotFound: true);
        m_Player1_LightKick = m_Player1.FindAction("LightKick", throwIfNotFound: true);
        m_Player1_HardPunch = m_Player1.FindAction("HardPunch", throwIfNotFound: true);
        m_Player1_HardKick = m_Player1.FindAction("HardKick", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player1
    private readonly InputActionMap m_Player1;
    private IPlayer1Actions m_Player1ActionsCallbackInterface;
    private readonly InputAction m_Player1_Move;
    private readonly InputAction m_Player1_LightPunch;
    private readonly InputAction m_Player1_LightKick;
    private readonly InputAction m_Player1_HardPunch;
    private readonly InputAction m_Player1_HardKick;
    public struct Player1Actions
    {
        private @InputManager m_Wrapper;
        public Player1Actions(@InputManager wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Player1_Move;
        public InputAction @LightPunch => m_Wrapper.m_Player1_LightPunch;
        public InputAction @LightKick => m_Wrapper.m_Player1_LightKick;
        public InputAction @HardPunch => m_Wrapper.m_Player1_HardPunch;
        public InputAction @HardKick => m_Wrapper.m_Player1_HardKick;
        public InputActionMap Get() { return m_Wrapper.m_Player1; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Player1Actions set) { return set.Get(); }
        public void SetCallbacks(IPlayer1Actions instance)
        {
            if (m_Wrapper.m_Player1ActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnMove;
                @LightPunch.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnLightPunch;
                @LightPunch.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnLightPunch;
                @LightPunch.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnLightPunch;
                @LightKick.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnLightKick;
                @LightKick.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnLightKick;
                @LightKick.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnLightKick;
                @HardPunch.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnHardPunch;
                @HardPunch.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnHardPunch;
                @HardPunch.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnHardPunch;
                @HardKick.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnHardKick;
                @HardKick.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnHardKick;
                @HardKick.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnHardKick;
            }
            m_Wrapper.m_Player1ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @LightPunch.started += instance.OnLightPunch;
                @LightPunch.performed += instance.OnLightPunch;
                @LightPunch.canceled += instance.OnLightPunch;
                @LightKick.started += instance.OnLightKick;
                @LightKick.performed += instance.OnLightKick;
                @LightKick.canceled += instance.OnLightKick;
                @HardPunch.started += instance.OnHardPunch;
                @HardPunch.performed += instance.OnHardPunch;
                @HardPunch.canceled += instance.OnHardPunch;
                @HardKick.started += instance.OnHardKick;
                @HardKick.performed += instance.OnHardKick;
                @HardKick.canceled += instance.OnHardKick;
            }
        }
    }
    public Player1Actions @Player1 => new Player1Actions(this);
    public interface IPlayer1Actions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnLightPunch(InputAction.CallbackContext context);
        void OnLightKick(InputAction.CallbackContext context);
        void OnHardPunch(InputAction.CallbackContext context);
        void OnHardKick(InputAction.CallbackContext context);
    }
}
