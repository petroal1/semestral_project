﻿using UnityEngine;
using UnityEditor;

public class GenCharAnimatorWindow : EditorWindow
{
    GameObject character;
    Animator charAnimator;
    string characterName = "";

    private readonly string stand = "stand";
    private readonly string crouch = "crouch";
    private readonly string jump = "jump";
    private readonly string lp = "lp";
    private readonly string lk = "lk";
    private readonly string hp = "hp";
    private readonly string hk = "hk";


    [MenuItem("Custom/Generate Character Animator Transitions")]
    static void Init()
    {
        GenCharAnimatorWindow window = (GenCharAnimatorWindow)GetWindow(typeof(GenCharAnimatorWindow));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        characterName = EditorGUILayout.TextField("Character Name", characterName);
        character = (GameObject)EditorGUILayout.ObjectField("Character GameObject", character, typeof(GameObject), true);
        if (character && character.GetComponent<Animator>() == null) {
            Debug.LogError("invalid character gameobject !!");
            character = null;
        }
        
        if (GUILayout.Button("Generate Character Animator Transitions"))
        {
            if (character == null)
            {
                Debug.LogError("invalid character gameobject !!");
            }
            else
            {
                charAnimator = character.GetComponent<Animator>();
                GenerateCharacterAnimator();
                Debug.Log("Animator transitions generation finished");
            }
        }

    }
    
    void GenerateCharacterAnimator()
    {
        // create values:
        //charAnimator.

        string stateName = "Base Layer.";

        stateName += characterName;
        stateName += "_";



    }
}
