﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public static InputManager inputMan;
    void Awake()
    {
        inputMan = new InputManager();
    }

    private void OnEnable()
    {
        inputMan.Enable();
    }
    private void OnDisable()
    {
        inputMan.Disable();
    }
}
