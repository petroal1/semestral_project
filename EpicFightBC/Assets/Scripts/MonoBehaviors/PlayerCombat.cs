﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    private Animator animator;
    private PlayerController playerCont;
    void Awake()
    {
        animator = GetComponent<Animator>();
        playerCont = GetComponent<PlayerController>();
        InputController.inputMan.Player1.LightKick.performed += _ => LightKick();
        InputController.inputMan.Player1.LightPunch.performed += _ => LightPunch();
        InputController.inputMan.Player1.HardKick.performed += _ => HardKick();
        InputController.inputMan.Player1.HardPunch.performed += _ => HardPunch();
    }

    private void LightKick()
    {
        animator.SetTrigger("LK");
    }
    private void LightPunch()
    {
        animator.SetTrigger("LP");
    }
    private void HardKick()
    {
        animator.SetTrigger("HK");
    }
    private void HardPunch()
    {
        animator.SetTrigger("HP");
    }
       

}
