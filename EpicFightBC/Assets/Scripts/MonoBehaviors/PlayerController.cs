﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEditor.Animations;

public class PlayerController : MonoBehaviour
{
    private Character character;
    private List<Move> moveList;
    public Move curMove;
    private Animator animator;
    
    public bool isBlocking;
    public bool isAttacking;
    public GameObject opponent;
    public float blockDistance = 1.5f;
    //public AnimatorController animCont;

    public Slider healthSlider;

    private bool canTakeDamage;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        moveList = new List<Move>();
        //AddBasicMoves();
        character = new Character("stickman", 350, moveList);

        healthSlider.value = character.CurrentHealth;
        healthSlider.minValue = 0;
        healthSlider.maxValue = character.Health;
        canTakeDamage = true;
    }

    //private void AddBasicMoves()
    //{
    //    ChildAnimatorState[] states = animCont.layers[0].stateMachine.states;

    //    List<string> attackStateNames = new List<string>();

    //    foreach (var state in states)
    //    {
    //        string name = state.state.name;
    //        if ((name[name.Length-1] == 'p' || name[name.Length-1] == 'k') && 
    //            (name[name.Length-2] == 'l' || name[name.Length-2] == 'h'))
    //        {
    //            attackStateNames.Add(name);
    //        }
    //    }

    //    foreach (var n in attackStateNames)
    //    {
    //        if (n[n.Length - 2] == 'l') moveList.Add(new Move(name, MoveData.DEF_LIGHT_ATTACK_DMG));
    //        else if (n[n.Length - 2] == 'h') moveList.Add(new Move(name, MoveData.DEF_HARD_ATTACK_DMG));
    //    }
    //}

    public IEnumerator Damage(int dmg)
    {
        canTakeDamage = false;
        animator.SetTrigger("Hit");
        character.CurrentHealth -= dmg;
        healthSlider.value = character.CurrentHealth;
        yield return new WaitForEndOfFrame();
        canTakeDamage = true;
    }

    public void ColliderTriggered(Collider2D collision)
    {
        if (collision.transform.IsChildOf(transform) || !canTakeDamage || isBlocking) return;

        Debug.Log(gameObject.name +  " was hit by " + collision.name);
        StartCoroutine(Damage(10));
    }
}
