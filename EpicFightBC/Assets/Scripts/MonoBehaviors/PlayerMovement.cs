﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    private PlayerController playerController;

    private Rigidbody2D rb;
    private Animator animator;
    private float speedX;
    private bool isCrouching;
    

    [SerializeField] private float moveSpeed = 50f;
    public LayerMask groundLayer;

    private readonly float movementSmoothing = 0.05f;
    private Vector3 curVelocity = Vector3.zero;

    void Awake()
    {
        playerController = GetComponent<PlayerController>();
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        InputController.inputMan.Player1.Move.performed += ctx => Move(ctx.ReadValue<Vector2>());
    }

    private void Move(Vector2 move)
    {
        speedX = move.x;

        isCrouching = move.y < 0 ? true : false;
        animator.SetBool("IsCrouching", isCrouching);

        if (move.y > 0)
        {
            animator.SetTrigger("Jump");
        }
    }
    
    private void FixedUpdate()
    {
        CheckBlocking();
        if (!isCrouching && !playerController.isBlocking) // move
        {
            Vector2 move = new Vector2(speedX * moveSpeed * Time.fixedDeltaTime, rb.velocity.y);
            rb.velocity = Vector3.SmoothDamp(rb.velocity, move, ref curVelocity, movementSmoothing);
            animator.SetFloat("Speed", speedX);
        }
    }    

    private void CheckBlocking()
    {
        // only block if the opponent is attacking !!!!!
        playerController.isBlocking = (speedX < 0 && playerController.opponent.GetComponent<PlayerController>().isAttacking) ? true : false;
        animator.SetBool("IsBlocking", playerController.isBlocking);
    }
}
