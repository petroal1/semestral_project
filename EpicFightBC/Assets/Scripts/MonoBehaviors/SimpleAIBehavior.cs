﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAIBehavior : MonoBehaviour
{
    public AIBehavior behavior;

    private Animator animator;
    private PlayerController playerCont;

    private void Start()
    {
        animator = GetComponent<Animator>();
        playerCont = GetComponent<PlayerController>();
    }

    void Update()
    {
        switch (behavior)
        {
            case AIBehavior.IDLE:
                playerCont.isBlocking = false;
                playerCont.isAttacking = false;
                animator.ResetTrigger("Jump");
                animator.ResetTrigger("LK");
                animator.SetBool("IsBlocking", false);
                break;
            case AIBehavior.BLOCKING:
                animator.SetBool("IsBlocking", true);
                playerCont.isBlocking = true;
                playerCont.isAttacking = false;
                break;
            case AIBehavior.JUMPING:
                playerCont.isBlocking = false;
                playerCont.isAttacking = false;
                animator.SetBool("IsBlocking", false);
                animator.SetTrigger("Jump");
                break;
            case AIBehavior.ATTACKING:
                playerCont.isBlocking = false;
                playerCont.isAttacking = true;
                animator.SetBool("IsBlocking", false);
                animator.SetTrigger("LK");
                break;
            default:
                break;
        }
    }
}
