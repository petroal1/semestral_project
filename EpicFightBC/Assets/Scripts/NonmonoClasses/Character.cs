﻿
using System.Collections.Generic;

public class Character
{
    public string Name { get; private set; }
    public int Health { get; private set; }
    public int CurrentHealth { get; set; }
    public List<Move> MoveList { get; private set; }

    public Character(string name, int health, List<Move> moveList)
    {
        Name = name;
        Health = health;
        CurrentHealth = health;
        MoveList = moveList;
    }

}
