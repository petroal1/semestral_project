﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move
{
    public string StateName { get; private set; }
    public int Damage { get; private set; }
    public int HitStunDuration { get; private set; }
    public int BlockStunDuration { get; private set; }

    public Move(string stateName, int damage)
    {
        StateName = stateName;
        Damage = damage;
    }

}
