﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MoveData
{
    public static readonly int DEF_LIGHT_ATTACK_DMG = 30;
    public static readonly int DEF_HARD_ATTACK_DMG = 90;
}
