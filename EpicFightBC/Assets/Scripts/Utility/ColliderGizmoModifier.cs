﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderGizmoModifier : MonoBehaviour
{
    public Color colliderColor = Color.blue;
    private Animator animator;

    private void Awake()
    {
        animator = GetComponentInParent<Animator>();
    }

    private void OnDrawGizmos()
    {
        Color border = colliderColor;
        if (animator != null && animator.GetBool("IsBlocking")) border = Color.blue;
        Color inside = border;
        inside.a = 0.2f;

        Vector2 thispos, colsize;
        
        BoxCollider2D[] cols = GetComponentsInChildren<BoxCollider2D>();
        foreach (var col in cols)
        {
            thispos = new Vector2(col.transform.position.x, col.transform.position.y);
            colsize = new Vector2(col.size.x * col.transform.localScale.x, col.size.y * col.transform.localScale.y);

            Gizmos.color = border;
            Gizmos.DrawWireCube(thispos + col.offset, colsize);
            Gizmos.color = inside;
            Gizmos.DrawCube(thispos + col.offset, colsize);
        }
        
    }
}
