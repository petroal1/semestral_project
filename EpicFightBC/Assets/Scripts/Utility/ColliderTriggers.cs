﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderTriggers : MonoBehaviour
{
    private PlayerController par;

    private void Start()
    {
        par = GetComponentInParent<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {       
        if (collision.tag == "OffensiveColliders" && tag == "DefensiveColliders")
        {
            par.ColliderTriggered(collision);
        }


    }
}
