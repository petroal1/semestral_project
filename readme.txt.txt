Zdrojove kody se nachazeji ve slozce EpicFightBC, pro jeho spusteni je treba nainstalovat game engine Unity,
posleze Unity otevrit - otevre se vam Unity Hub a tam je v tabu "Projects" treba kliknout na "Add" a tim tam pridat tu slozku EpicFightBC, pak az to lze spustit.

Navod ke spusteni buildu:
K tomu eni nic treba, staci ve slozce EpicFightBC/Builds spustit soubor EpicFightBC.exe

Ovladani hry:
pohyb postavy je ovladan sipkami (left, right je chozeni, up je skok, down je prikrceni)
utoci se klavesami Ctrl, Alt, Z a X

Zaverecna zprava k projektu se nachazi v souboru Semestral_Project___Epic_Fight.pdf, Design Document ke hre je v souboru DD___Epic_Fight.pdf